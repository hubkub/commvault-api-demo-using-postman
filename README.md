# Commvault API demo using Postman
1. Import json collection to Postman

2. Change variables in Pre-request Script of Login:

```
// set your CS username and password here
// example:
// hubert@sys:~$ echo -n "P@ssw0rd" |base64
// UEBzc3cwcmQ=

postman.setEnvironmentVariable("userName", "admin");
postman.setEnvironmentVariable("password", "UEBzc3cwcmQ=");
pm.environment.get("variable_key");

// Set your IP of CommServe
postman.setEnvironmentVariable("ServerUrl", "https://192.168.100.100/commandcenter/api");
```


## More api collections
https://api.commvault.com or https://github.com/Commvault/Rest-API-Postman-Collection
